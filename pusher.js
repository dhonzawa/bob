const fs = require('fs');
const request = require('request');

function pushCommit() {
    destination = "D:/dhonzawa/Desktop/laia/newBuild/"

    //requires colors.js file to be ready
    const iconFile = fs.readFileSync(destination + "icon.png", { encoding: 'base64' });
    const launcherFile = fs.readFileSync(destination + "launcher.png", { encoding: 'base64' });
    const logoFile = fs.readFileSync(destination + "logo.png", { encoding: 'base64' });
    appJson = JSON.parse(fs.readFileSync(destination + "app.json", 'utf8'));
    //read googleservice and fcm.services
    const googleService = fs.readFileSync(destination + "GoogleService-Info.plist", "utf-8");
    const fcmService = fs.readFileSync(destination + "fcm.services.json", 'utf-8');
    const colorFile = fs.readFileSync(destination + "colors.js", 'utf-8');
    JSONpayload = JSON.stringify(appJson)
    //newProjectID
    var options = {
        'method': 'POST',
        'url': 'https://gitlab.com/api/v4/projects/' + 40329617 + '/repository/commits',
        'headers': {
            'PRIVATE-TOKEN': 'glpat--AeCn4ATtU6Xi_awzwoz',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "branch": "master",
            "commit_message": "feat: initial customization, rebuild -Bob :)",
            "actions": [
                {
                    "action": "update",
                    "file_path": "assets/variables/icon.png",
                    "encoding": "base64",
                    "content": iconFile
                },
                {
                    "action": "update",
                    "file_path": "assets/variables/launcher.png",
                    "encoding": "base64",
                    "content": launcherFile
                },
                {
                    "action": "update",
                    "file_path": "assets/variables/logo.png",
                    "encoding": "base64",
                    "content": logoFile
                },
                {
                    "action": "update",
                    "file_path": "app.json",
                    "encoding": "text",
                    "content": JSONpayload
                },
                {
                    "action": "create",
                    "file_path": "fcm.services.json",
                    "encoding": "text",
                    "content": googleService
                },
                {
                    "action": "update",
                    "file_path": "GoogleService-Info.plist",
                    "encoding": "text",
                    "content": fcmService
                },
                {
                    "action": "update",
                    "file_path": "build/colors.js",
                    "encoding": "text",
                    "content": colorFile
                }
            ]
        })

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
    });

}
pushCommit()