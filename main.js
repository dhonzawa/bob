const fs = require('fs');
const request = require('request');
path = "D:/dhonzawa/Desktop/laia/filesNeeded/"
destination = "D:/dhonzawa/Desktop/laia/newBuild/"
appJSON = {}
var appJsonFile
var colorFile
var projectName = ""
newProjectID = ""
//contains all data need to customize the app that's behind equal signs, (API keys, etc)
const appInfo = [{ setupName: "Project Name", stdName: "slug" }, { setupName: "Official", stdName: "name" },
{ setupName: "GoogleMaps", stdName: "apiKey" }, { setupName: "bundle", stdName: "package" }, { setupName: "bundle", stdName: "bundleidentifier" },
{ setupName: "#Short App", stdName: "description" }] //appInfo.splitName is the slug of the project btw
//contains all customizable colors
const customColors = [{ name: "authBkg" }, { name: "authSubtitle" }, { name: "authFieldColor" }, { name: "authFieldInput" }, { name: "authBtnBkg" },
{ name: "authBtnBorder" }, { name: "authBtnTxt" }, { name: "authLgnPasLinks" }, { name: "authTnC" }, { name: "authSpinner" }]
//filenames for the variable images :D
const variableFiles = ["icon.png", "launcher.png", "logo.png"]


//options for the JSON payload needed in the import/export process
const options = {
    url: 'https://gitlab.com/api/v4/projects/21161726/export',
    headers: { 'PRIVATE-TOKEN': 'glpat--AeCn4ATtU6Xi_awzwoz' },
}

function modifyAppJSON() {
    console.log(appInfo)
    appInfo.forEach(value => {
        if (value.stdName === 'slug') {
            projectName = value.splitName
            appJsonFile.expo.slug = value.splitName
            appJsonFile.expo.android.package = "com.creativegroup." + value.splitName
            appJsonFile.expo.ios.bundleIdentifier = "com.creativegroup." + value.splitName
            appJsonFile.expo.hooks.postPublish[0].config.project = value.splitName
        }
        if (value.stdName === "description") {
            appJsonFile.expo.description = value.splitName
        }
        if (value.stdName === "apiKey") {
            //console.log(appJsonFile.expo.android.config.googleMaps.apiKey)
            appJsonFile.expo.android.config.googleMaps.apiKey = value.splitName
            appJsonFile.expo.ios.config.googleMapsApiKey = value.splitName
            /*
            this is not working, I think it might be related to the changes EA2 is going through?
            appJsonFile.expo.web.config.firebase.apiKey = value.splitName
            */
        }
        fs.writeFileSync(destination + "app.json", JSON.stringify(appJsonFile), 'utf8');

    })
    modifyColorsFile()

}

function modifyColorsFile() {
    customColors.forEach(color => {
        for (let line = 1; line < colorFile.length - 4; line++) {
            if (colorFile[line].includes(color.name)) {
                temp = colorFile[line].split("//")
                temp[0] = "    " + color.name + ": " + color.color
                colorFile[line] = temp[0] + " //" + temp[1]
            }
        }

    })
    //now just write a file
    colorFile.forEach(line => {
        fs.appendFileSync(destination + "colors.js", line.toString(), 'utf8');
    })
    googleServicePlist()
}

function readAppJson() {
    appJsonFile = JSON.parse(fs.readFileSync(path + "templateApp.json", 'utf8'));
    readColorsJS()
}

function readAppSetupFile() {

    var appSetupFile = fs.readFileSync(path + "app-setup.txt").toString().split("\n");
    //iterate through file, put each line into array 
    appSetupFile.forEach(line => {
        //iterate through lines, split them if they contain an equal sign (new format for app-setup.txt?)
        appInfo.forEach(appKeys => {
            if (line.includes(appKeys.setupName)) {
                //push an object with the following keys: line.split("=")
                a = line.split("=")
                appKeys.splitName = a[1]


            }
        })
        //reads the colors
        customColors.forEach(colorSetting => {
            if (line.includes(colorSetting.name)) {
                //if line contains then split it by the ":" and push it
                colorSetting.color = line.split(":")[1]
            }

        })
    });
    readAppJson()
}

function googleServicePlist() {
    //moves GoogleService-Info.plist to /newBuild
    //renames google-services.json to fcm.services.json and moves it to newbuild
    let variableFiles = ["GoogleService-Info.plist", "google-services.json"]
    variableFiles.forEach(file => {
        if (fs.existsSync(path + file)) {
        } else {
            throw new Error(file + " does not exist");
        }
    })
    fs.renameSync(path + "GoogleService-Info.plist", destination + "GoogleService-Info.plist")
    fs.renameSync(path + "google-services.json", destination + "fcm.services.json")
    console.log("parser.js done!")
    pngChecker()


}

function readColorsJS() {
    //reads the files
    colorFile = fs.readFileSync(path + "colorTemplate.js").toString().split("/n");
    modifyAppJSON()
}
/*
</PARSER.JS>
<pngChecker.js>
*/
function pngChecker() {
    var files = fs.readdirSync(path);
    var fileSizes = []

    //check if the variable files exist
    for (let x = 0; x < variableFiles.length; x++) {

        if (fs.existsSync(path + "/" + variableFiles[x])) {
            console.log(variableFiles[x] + " exists!!")

        } else {
            console.log()
            throw new Error(variableFiles[x] + " does not exist");
        }
        if (x === 2) {
            checkFileSize()
        }
    }
}
function checkFileSize() {
    for (let x = 0; x < variableFiles.length; x++) {
        var stats = fs.statSync(path + "/" + variableFiles[x])
        var fileBuffer = Buffer.from(path + "/" + variableFiles[x])
        var fileSizeInBytes = stats.size;
        // Convert the file size to megabytes (optional)
        var fileSizeInMegabytes = fileSizeInBytes / (1024 * 1024);
        if (fileSizeInMegabytes > 5) {
            throw new Error(variableFiles[x] + " is too big");

        } else {
            console.log(variableFiles[x] + " " + fileSizeInMegabytes)

        }
    }
    moveImages()
}
//it moves files :) 
function moveImages() {
    let newPath = "D:/dhonzawa/Desktop/laia/newBuild"
    variableFiles.forEach(file => {
        fs.rename(path + "/" + file, newPath + "/" + file, function (err) {
            if (err) throw err
        })
    })
    console.log("Images are checked")
    createNewExport()

}
/*
</pngChecker.js>
<bob(import/export)>
*/

//makes POST request to create a new export
function createNewExport() {
    request.post(options, function (error, response, body) {
        if (!error && response.statusCode === 202) {
            console.log(body) // Print the req.body
            requestStatus() //calls the export status check
        }
    })
}

//check the export status
function requestStatus() {
    request(options, function (error, response, body) {
        if (!error) {
            checkResponse(body)
        }

    })
}
function checkResponse(body) {
    body = JSON.parse(body)
    console.log(body.export_status)
    if (body.export_status != 'finished') {
        //console.log(body.export_status)
        setTimeout(function () {
            requestStatus();
            console.log('request sent after 1 minute.')
        }, 60000);

    } else {
        console.log("it's done! " + body.export_status)
        retrieveExportFile()
    }
}

function retrieveExportFile() {
    options.url = "https://gitlab.com/api/v4/projects/21161726/export/download"
    request(options, function (error, response, body) {
        if (!error) {
            sendExportFile()
        }
    }).pipe(fs.createWriteStream('projectFile.tar.gz'))
    console.log("projectFile has been retrieved")
}

function sendExportFile() {
    console.log(projectName + " ooo")
    //gitlabPath = "https://gitlab.com/creativegroup/event-app-2/events/" + projectName
    var importOptions = {
        'method': 'POST',
        'url': 'https://gitlab.com/api/v4/projects/import',
        'headers': {
            'PRIVATE-TOKEN': 'glpat--AeCn4ATtU6Xi_awzwoz'
        },
        formData: {
            'file': {
                'value': fs.createReadStream('D:/dhonzawa/Desktop/laia/projectFile.tar.gz'),
                'options': {
                    'filename': 'D:/dhonzawa/Desktop/laia/projectFile.tar.gz',
                    'contentType': null
                }
            },
            'path': projectName,
            'namespace': 'creativegroup/event-app-2/events'

        }
    };
    request(importOptions, function (error, response) {
        if (error) throw new Error(error);
        let apiResponse = JSON.parse(response.body)
        newProjectID = apiResponse.id
        console.log(apiResponse)
        retrieveVars()
    });

}


/*
</bob>
<createVars>
this creates all the necessary variables to have working CI/CD
*/
gitlabVariables = [
    { setupName: "ASC_APP_ID" },
    { setupName: "APP_SPECIFIC_PASSWORD" },
    { setupName: "CLIENT_ID" },
    { setupName: "SENTRY_DSN" }
]
//ok so fyi we are gonna parse the app-setup file and get the variables 
/* variable example
{
    "variable_type": "env_var",
    "key": gitlabVariables[0].setupName,
    "value": gitlabVariables[0].value,
    "protected": false,
    "masked": false,
    "environment_scope": "*"
}
*/

function readENV() {
    if (fs.existsSync(path + ".env")) {
        envFile = fs.readFileSync(path + ".env", "utf8")
        sendVars()

    } else {
        throw new Error("There's no .env file D:");
    }

}

function sendVars() {
    gitlabVariables.forEach(item => {
        if (item.setupName === "CLIENT_ID") {
            isProtected = false
        } else {
            isProtected = true
        }
        var options = {
            'method': 'POST',
            'url': 'https://gitlab.com/api/v4/projects/' + newProjectID + "/variables",
            'headers': {
                'PRIVATE-TOKEN': 'glpat--AeCn4ATtU6Xi_awzwoz',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "variable_type": "env_var",
                "key": item.setupName,
                "value": item.value,
                "protected": isProtected,
                "masked": false,
                "environment_scope": "*"
            })
        };
        //im not making a request for each variable bc I want to btw

        request(options, function (error, response) {
            if (error) throw new Error(error);
            //console.log(response.body);
        });

        //now send both files, eas and .env     
        options.body = JSON.stringify({
            "variable_type": "file",
            "key": "ENV_MASTER",
            "value": envFile,
            "protected": true,
            "masked": false,
            "environment_scope": "*"
        })
        request(options, function (error, response) {
            if (error) throw new Error(error);
            //console.log(response.body);
        });
        options.body = JSON.stringify({
            "variable_type": "file",
            "key": "EAS_JSON",
            "value": appJsonFile,
            "protected": true,
            "masked": false,
            "environment_scope": "*"
        })

    })
    console.log("CI/CD has been set up")
    pushCommit()

}

function readJSON() {
    //will parse file and send eas.json
    appJsonFile = JSON.parse(fs.readFileSync(path + "easTemplate.json", 'utf8'));
    readENV()
}

function retrieveVars(apiResponse) {
    var appSetupFile = fs.readFileSync(path + "app-setup.txt").toString().split("/n");
    appSetupFile.forEach(line => {
        gitlabVariables.forEach(item => {
            if (line.includes(item.setupName)) {
                item.value = line.split("=")[1]
            }
        })

    });
    //iterate through file, put each value into object yadda yadda
    readJSON()
}
/*
</createVars>
<pushCommit>
*/
function pushCommit() {
    //requires colors.js file to be ready
    const iconFile = fs.readFileSync(destination + "icon.png", { encoding: 'base64' });
    const launcherFile = fs.readFileSync(destination + "launcher.png", { encoding: 'base64' });
    const logoFile = fs.readFileSync(destination + "logo.png", { encoding: 'base64' });
    appJson = JSON.parse(fs.readFileSync(destination + "app.json", 'utf8'));
    //read googleservice and fcm.services
    const googleService = fs.readFileSync(destination + "GoogleService-Info.plist", "utf-8");
    const fcmService = fs.readFileSync(destination + "fcm.services.json", 'utf-8');
    const colorFile = fs.readFileSync(destination + "colors.js", 'utf-8');
    JSONpayload = JSON.stringify(appJson)
    //newProjectID
    var options = {
        'method': 'POST',
        'url': 'https://gitlab.com/api/v4/projects/' + newProjectID + '/repository/commits',
        'headers': {
            'PRIVATE-TOKEN': 'glpat--AeCn4ATtU6Xi_awzwoz',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "branch": "master",
            "commit_message": "feat: initial customization, rebuild -Bob :)",
            "actions": [
                {
                    "action": "update",
                    "file_path": "assets/variables/icon.png",
                    "encoding": "base64",
                    "content": iconFile
                },
                {
                    "action": "update",
                    "file_path": "assets/variables/launcher.png",
                    "encoding": "base64",
                    "content": launcherFile
                },
                {
                    "action": "update",
                    "file_path": "assets/variables/logo.png",
                    "encoding": "base64",
                    "content": logoFile
                },
                {
                    "action": "update",
                    "file_path": "app.json",
                    "encoding": "text",
                    "content": JSONpayload
                },
                {
                    "action": "create",
                    "file_path": "fcm.services.json",
                    "encoding": "text",
                    "content": googleService
                },
                {
                    "action": "update",
                    "file_path": "GoogleService-Info.plist",
                    "encoding": "text",
                    "content": fcmService
                },
                {
                    "action": "update",
                    "file_path": "build/colors.js",
                    "encoding": "text",
                    "content": colorFile
                }
            ]
        })

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        let apiResponse = JSON.parse(response.body)
        if (apiResponse.message.includes(newProjectID)) {
            console.log(apiResponse)
        }
        else if (apiResponse.message.includes("404")) {
            console.log(apiResponse)

            pushCommit()
        } else if (apiResponse.message.includes("500")) {
            console.log(apiResponse)

            pushCommit()
        } else if (apiResponse.message.includes("A file with this name doesn't exist")) {
            console.log(apiResponse)
            pushCommit()
        } else {
            console.log(apiResponse)

        }
    });
}


//contains all function calls to kick off automation
readAppSetupFile()