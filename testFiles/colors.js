const customColors = {
    main: '#56c8df', // Highlight color (buttons, actions)
    bkg: '#fff', // App background
  
    textDark: '#1b2e43', // Main text color
    textLight: '#92989d', // Secondary text color
    textReverse: '#fff', // Text color when the background is the main color (like filled buttons)
    notification: '#ec2268', // Notification badge color
  
    lightHeader: true, // OS status bar text color. true = black, false = white
    header: '#fff', // Header background color
    headerText: '#1b2e43', // Header text/icon color
    headerAction: '#3ab5ce', // Color for the header's primary action, when applicable (save, done, confirm, etc)
  
    lightHeaderHome: true, // Home screen OS status bar text color. true = black, false = white
    headerHome: '#fff', // Home screen header background color
    headerHomeText: '#1b2e43', // Home screen header text/icon color
  
    textLink: 'blue', // Link color
  
    acceptColor: '#33cd5f', // Accept btn or text
    denyColor: '#ef473a', // Deny btn or text
  
    //Color settings for all of the Login and Forgot password screens
    authBkg:  '#aaa',  // Login and Forgot Password background color
    authSubtitle:  '#aaa', // Text above the fields on Login and Forgot Password screens
    authFieldColor:  '#aaa',  // Text color for fields on Login and Forgot Password
    authFieldInput:  '#aaa', // Text input color for fields on Login and Forgot Password
    authBtnBkg:  '##aaa',  // Background color for Login and Reset password buttons
    authBtnBorder:  '#aaa',  // Border color for Login and Reset password buttons
    authBtnTxt:  '#fff',  // Border color for Login and Reset password buttons
    authLgnPasLinks:  '#aaa',  // Link color for Login instead and Forgot password link
    authTnC:  '#aaa', // Text color for T&C and link
    authSpinner: '#000', // Color of the Spinner
  };